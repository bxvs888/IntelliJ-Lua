Lua IDE for IntelliJ IDEA

[Download](/../../releases)

![snapshot](http://git.oschina.net/uploads/images/2017/0205/154051_382def6d_5199.png)

feature | progress
------- | -------
Syntax highlighting | ok
Highlighting Global | ok
Highlighting local/param | ok
Find usages | ok
Rename(Shift + F6) | ok
Go to definition(Ctrl + Mouse) | ok
Go to symbol(Ctrl + Alt + Shift + N) | ok
Go to class(Ctrl + N) | ok
Go to file(Ctrl + Shift + N) | ok
Go to definition | ok
Keyword completion | ok
Basic completion | ok
Structure view | ok
Brace Matching | ok
Comment in/out | ok
Customizable highlighting page | ok
Doc based type/class annotation | ok
Name suggestion for refactor | 60%
Modules support | 80%
Quick Documentation(Ctrl + Q) | 50%
Live templates | 10%
Postfix completion templates | 10%
Code formatter | 90%
Code intentions | 20%
Code inspections | 20%
Lua Standard Library/API | 50%
Debugger | 50%
Module support | 0%
Lua 5.3 | 0%
... .etc |